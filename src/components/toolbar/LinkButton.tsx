import { useSlate } from 'slate-react';
import { useCallback } from 'react';
import CustomEditor from '../../lib/editor';

export function LinkButton() {
  const editor = useSlate();

  const handleToggleLink = useCallback(() => {
    if (CustomEditor.isLinkMarkActive(editor)) {
      CustomEditor.removeLinkMark(editor);
      return;
    }

    const link = prompt('Link');
    if (!link.trim()) return;

    CustomEditor.setLinkMark(editor, link);
  }, [editor]);

  return (
    <button onClick={handleToggleLink}>
      <i className="fa-solid fa-link"/>
    </button>
  );
}
