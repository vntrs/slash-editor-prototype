import { useSlate } from 'slate-react';
import { useCallback } from 'react';
import CustomEditor from '../../lib/editor';

export function FontFamilyButton({ families }) {
  const editor = useSlate();

  const handleSetFont = useCallback((event) => {
    const { value: font } = event.target;
    CustomEditor.setFontMark(editor, font);
  }, [editor]);
  return (
    <select name="font-family" onChange={handleSetFont}>
      {families.map(family => (
        <option key={`font-family:${family}`} value={family}>
          {family}
        </option>
      ))}
    </select>
  );
}
