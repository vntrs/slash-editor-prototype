import { useSlate } from 'slate-react';
import { useCallback } from 'react';
import CustomEditor from '../../lib/editor';

export function ItalicButton() {
  const editor = useSlate();

  const handleToggleItalic = useCallback(
    () => CustomEditor.toggleItalicMark(editor),
    [editor],
  );
  return (
    <button onClick={handleToggleItalic}>
      <i className="fa-solid fa-italic" />
    </button>
  );
}
