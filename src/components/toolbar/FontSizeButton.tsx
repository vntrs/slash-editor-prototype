import { useSlate } from 'slate-react';
import { useCallback } from 'react';
import CustomEditor from '../../lib/editor';

export function FontSizeButton({ sizes }) {
  const editor = useSlate();

  const handleSetFontSize = useCallback((event) => {
    const { value: fontSize } = event.target;
    console.log('setting font size to', fontSize);
    CustomEditor.setFontSizeMark(editor, fontSize);
  }, [editor]);

  return (
    <select name="font-size" onChange={handleSetFontSize}>
      {sizes.map(size => (
        <option key={`font-size:${size}`} value={size}>
          {size}px
        </option>
      ))}
    </select>
  );
}
