import { useSlate } from 'slate-react';
import { useCallback } from 'react';
import CustomEditor from '../../lib/editor';

export function BoldButton() {
  const editor = useSlate();

  const handleToggleBold = useCallback(
    () => CustomEditor.toggleBoldMark(editor),
    [editor],
  );

  return (
    <button onClick={handleToggleBold}>
      <i className="fa-solid fa-bold"/>
    </button>
  );
}
