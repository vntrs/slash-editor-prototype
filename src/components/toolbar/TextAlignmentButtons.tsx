import { useSlate } from 'slate-react';
import { useCallback } from 'react';
import CustomEditor from '../../lib/editor';

export function TextAlignmentButtons() {
  const editor = useSlate();
  const handleSetAlignment = useCallback(
    (align) => CustomEditor.setTextAlignment(editor, align),
    [editor],
  );

  return (
    <>
      <button onClick={() => handleSetAlignment('left')}>
        <i className="fa-solid fa-align-left" />
      </button>
      <button onClick={() => handleSetAlignment('center')}>
        <i className="fa-solid fa-align-center" />
      </button>
      <button onClick={() => handleSetAlignment('right')}>
        <i className="fa-solid fa-align-right" />
      </button>
      <button onClick={() => handleSetAlignment('justify')}>
        <i className="fa-solid fa-align-justify" />
      </button>
    </>
  );
}
