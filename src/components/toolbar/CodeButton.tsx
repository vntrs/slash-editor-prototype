import { useCallback } from 'react';
import CustomEditor from '../../lib/editor';
import { useSlate } from 'slate-react';

export function CodeButton() {
  const editor = useSlate();

  const handleToggleCode = useCallback(
    () => CustomEditor.toggleCodeBlock(editor),
    [editor],
  );

  return (
    <button onClick={handleToggleCode}>
      <i className="fa-solid fa-code"/>
    </button>
  );
}
