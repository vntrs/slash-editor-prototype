import CustomEditor from '../../lib/editor';
import { useCallback } from 'react';
import { useSlate } from 'slate-react';

export function StrikeButton() {
  const editor = useSlate();

  const handleToggleStrike = useCallback(
    () => CustomEditor.toggleStrikeMark(editor),
    [editor],
  );

  return (
    <button onClick={handleToggleStrike}>
      <i className="fa-solid fa-strikethrough" />
    </button>
  );
}
