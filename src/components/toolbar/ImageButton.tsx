import { useCallback } from 'react';
import CustomEditor from '../../lib/editor';
import { useSlate } from 'slate-react';

export function ImageButton() {
  const editor = useSlate();
  const handleAddImage = useCallback(() => {
    const image = prompt('Image URL');
    if (!image.trim()) return;

    CustomEditor.insertImage(editor, image);
  }, [editor]);

  return (
    <button onClick={handleAddImage}>
      <i className="fa-regular fa-image"/>
    </button>
  );
}
