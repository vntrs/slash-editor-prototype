import styled, { css } from 'styled-components/macro';
import { useFileContext } from '../context/file-context';
import { useCallback } from 'react';
import { useEditorContext } from '../context/editor-context';

const Container = styled.div`
  display: flex;
  flex-direction: column;
  flex-grow: 1;
  flex-basis: 0;
`;

const File = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: .5em 1em;
  cursor: pointer;
  color: white;

  &:hover {
    background-color: rgba(255, 255, 255, 0.25);
  }
  
  ${({ active }) => {
    if (!active) return css``;
    
    return css`
      background-color: rgba(255, 255, 255, 0.5);
    `;
  }}
`;

export function Sidebar() {
  const { files, createFile, removeFile } = useFileContext();
  const { active, setActive } = useEditorContext();

  const handleCreateFile = useCallback(() => {
    const filename = prompt('Filename');
    if (!filename.trim()) return;

    createFile(filename);
  }, [createFile]);

  const handleRemoveFile = useCallback((filename: string) => {
    const confirmed = confirm(`Are you sure you want to delete ${filename}?`);
    if (!confirmed) return;

    removeFile(filename);
  }, [removeFile]);

  const isFileActive = useCallback((filename: string) => {
    if (!active) return false;
    return filename === active;
  }, [active]);

  return (
    <Container>
      <button onClick={handleCreateFile}>Create new file</button>
      {files.map((filename) => (
        <File key={filename} onClick={() => setActive(filename)} active={isFileActive(filename)}>
          <span>{filename}</span>
          <button onClick={() => handleRemoveFile(filename)}>Remove</button>
        </File>
      ))}
    </Container>
  );
}
