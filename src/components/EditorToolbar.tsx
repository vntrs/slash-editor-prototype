import styled from 'styled-components/macro';

import { CodeButton } from './toolbar/CodeButton';
import { BoldButton } from './toolbar/BoldButton';
import { ItalicButton } from './toolbar/ItalicButton';
import { StrikeButton } from './toolbar/StrikeButton';
import { LinkButton } from './toolbar/LinkButton';
import { ImageButton } from './toolbar/ImageButton';
import { FontSizeButton } from './toolbar/FontSizeButton';
import { FontFamilyButton } from './toolbar/FontFamilyButton';
import { TextAlignmentButtons } from './toolbar/TextAlignmentButtons';

const Container = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: stretch;
  gap: 4px;

  button {
    box-sizing: border-box;
  }

  select {
    margin: 0;
    box-sizing: border-box;
  }
`;

export function EditorToolbar() {
  return (
    <Container>
      <FontSizeButton sizes={[8, 12, 14, 16, 20, 24, 32, 40, 48]} />
      <FontFamilyButton families={['serif', 'sans-serif', 'monospace']} />
      <TextAlignmentButtons />
      <BoldButton />
      <ItalicButton />
      <StrikeButton />
      <LinkButton />
      <ImageButton />
      <CodeButton />
    </Container>
  );
}

