import { useCallback, useEffect, useState } from 'react';
import { createEditor } from 'slate';
import { Editable, Slate, withReact } from 'slate-react';
import styled from 'styled-components/macro';

import { EditorToolbar } from './EditorToolbar';
import type { CustomElement } from '../lib/editor-elements';
import CustomEditor, { withCustomEditor } from '../lib/editor';
import { useEditorRenderers } from '../lib/elements';
import { useEditorContext } from '../context/editor-context';

const EditorWrapper = styled.div`
  display: flex;
  flex-direction: column;
  flex-grow: 3;
  background-color: #c0c0c0;
  flex-basis: 0;
`;

const Container = styled.div`
  padding: 1em 2em;
  background-color: white;
  flex-grow: 1;
`;

const EditorTopBar = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 4px 8px;
  font-family: monospace;
`;

const Center = styled.div`
  display: flex;
  flex-grow: 1;
  justify-content: center;
  align-items: center;
  font-size: 2em;
`;

const initialContent: CustomElement[] = [
  {
    type: 'paragraph', children: [
      { text: '' },
    ],
  } as CustomElement,
];

export function Editor() {
  const { active, content, updateContent } = useEditorContext();
  const { renderElement, renderLeaf } = useEditorRenderers();
  const [editor] = useState(() => {
    const e = withReact(createEditor());

    return withCustomEditor(e);
  });

  const handleKeyDown = useCallback((event) => {
    if (!event.ctrlKey) return;
    switch (event.key) {
      case '`': {
        event.preventDefault();
        CustomEditor.toggleCodeBlock(editor);
        return;
      }
      case 'b': {
        event.preventDefault();
        CustomEditor.toggleBoldMark(editor);
        return;
      }
    }
  }, [editor]);

  useEffect(() => {
    if (!active && editor.children.length) {
      editor.children = initialContent;
      editor.onChange();
      return;
    }

    if (!content) return;
    editor.children = content;
    editor.onChange();
  }, [editor, content]);

  if (!active) {
    return (
      <EditorWrapper>
        <Center>Open a file from the left</Center>
      </EditorWrapper>
    );
  }

  return (
    <Slate
      editor={editor}
      onChange={updateContent}
      value={content ?? []}>
      <EditorWrapper>
        <EditorTopBar>
          <EditorToolbar />
          <span>{active}</span>
        </EditorTopBar>
        <Container>
          <Editable
            renderElement={renderElement}
            renderLeaf={renderLeaf}
            onKeyDown={handleKeyDown}
          />
        </Container>
      </EditorWrapper>
    </Slate>
  );
}
