import React, { useCallback, useContext, useEffect, useState } from 'react';
import { CustomElement } from '../lib/editor-elements';

type EditorContextType = {
  active: string | null;
  content: CustomElement[] | null;
  setActive: (filename: string) => void;
  updateContent: (content: CustomElement[]) => void;
};
const EditorContext = React.createContext<EditorContextType>({
  active: null,
  content: null,
  setActive: () => undefined,
  updateContent: () => undefined,
});

export const useEditorContext = () => useContext(EditorContext);

export function EditorContextProvider({ children }) {
  const [active, setActive] = useState<string>(null);
  const [content, setContent] = useState<CustomElement[]>(null);

  const handleSetActive = useCallback((filename: string) => {
    const content = localStorage.getItem(`editor:files:${filename}`);
    if (!content) {
      setContent([
        {
          type: 'paragraph', children: [
            { text: '' },
          ],
        } as CustomElement,
      ]);
      setActive(filename);
      return;
    }

    setContent(JSON.parse(content));
    setActive(filename);
  }, [setActive, setContent]);

  const updateContent = useCallback(
    (content: CustomElement[]) => setContent(content),
    [setContent],
  );

  useEffect(() => {
    if (!active) return;
    if (!content) return;

    const json = JSON.stringify(content);
    localStorage.setItem(`editor:files:${active}`, json);
  }, [active, content]);

  return (
    <EditorContext.Provider value={{ active, content, setActive: handleSetActive, updateContent }}>
      {children}
    </EditorContext.Provider>
  );
}
