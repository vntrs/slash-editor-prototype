import React, { useCallback, useContext, useEffect, useState } from 'react';

type FileContextType = {
  files: string[];
  fileExists: (filename: string) => boolean;
  createFile: (filename: string) => void;
  removeFile: (filename: string) => void;
};
const FileContext = React.createContext<FileContextType>({
  files: [],
  fileExists: () => false,
  createFile: () => undefined,
  removeFile: () => undefined,
});

export const useFileContext = () => useContext(FileContext);

function loadFilesFromLocalStorage(): string[] {
  const content = localStorage.getItem('editor:files');
  if (!content) return [];

  return JSON.parse(content);
}

export function FileContextProvider({ children }) {
  const [files, setFiles] = useState(loadFilesFromLocalStorage);

  const fileExists = useCallback(
    (filename: string) => files.some((f) => f === filename),
    [files],
  );
  const createFile = useCallback((filename: string) => {
    if (fileExists(filename)) return;

    setFiles([...files, filename]);
  }, [files, setFiles]);
  const removeFile = useCallback((filename: string) => {
    if (!fileExists(filename)) return;

    setFiles(
      files.filter(f => f !== filename),
    );
  }, [files, setFiles]);

  useEffect(
    () => localStorage.setItem('editor:files', JSON.stringify(files)),
    [files],
  );

  return (
    <FileContext.Provider value={{ files, fileExists, createFile, removeFile }}>
      {children}
    </FileContext.Provider>
  );
}
