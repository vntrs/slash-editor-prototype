import { Editor } from 'slate';

import { EditorWithImages, withImages } from './plugins/image-plugin';
import { EditorWithTextStyle } from './plugins/text-style-plugin';
import { EditorWithLinks } from './plugins/link-plugin';
import { EditorWithTextFont } from './plugins/text-font-plugin';
import { EditorWithCodeBlocks } from './plugins/code-plugin';
import { EditorWithTextAlignment } from './plugins/text-alignment-plugin';

const CustomEditor = {
  ...Editor,
  ...EditorWithCodeBlocks,
  ...EditorWithTextAlignment,
  ...EditorWithTextStyle,
  ...EditorWithTextFont,
  ...EditorWithLinks,
  ...EditorWithImages,
};

function withEditorMiddlewares(editor, middlewares) {
  if (middlewares.length === 0) return editor;

  const [withMiddleware, ...remaining] = middlewares;
  return withEditorMiddlewares(
    withMiddleware(editor),
    remaining,
  );
}

export function withCustomEditor(editor) {
  return withEditorMiddlewares(editor, [
    withImages,
  ]);
}

export default CustomEditor;
