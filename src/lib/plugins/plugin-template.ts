// noinspection JSUnusedGlobalSymbols
import { BaseEditor } from 'slate';

/**
 * Add "static methods" for the editor here,
 * which will be spread into the CustomEditor
 */
export const EditorWithPlugin = {};

/**
 * Override or intercept existing logic for the editor here
 * @param {BaseEditor} editor
 */
export function withPlugin(editor: BaseEditor) {
  const { isVoid } = editor;
  editor.isVoid = (element) => {
    return isVoid(element);
  };

  return editor;
}
