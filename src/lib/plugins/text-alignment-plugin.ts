import { Editor, Transforms } from 'slate';
import { CustomAlignedElement } from '../editor-elements';

export const EditorWithTextAlignment = {
  getTextAlignment(editor) {
    const [match] = Editor.nodes(editor, {
      match: ({ align }) => typeof align === 'string',
    });

    return match ? match.align : 'left';
  },

  setTextAlignment(editor, align: 'left' | 'center' | 'right' | 'justify') {
    const currentAlignment = EditorWithTextAlignment.getTextAlignment(editor);
    if (currentAlignment === align) return;

    Transforms.setNodes(
      editor,
      { align } as CustomAlignedElement,
      { match: n => Editor.isBlock(editor, n) },
    );
  },
};
