import { Editor, Text, Transforms } from 'slate';
import { CustomElement, CustomText } from '../editor-elements';

export const EditorWithTextFont = {
  isFontMarkActive(editor) {
    const [match] = Editor.nodes(editor, {
      match: ({ font }) => typeof font === 'string',
      universal: true,
    });

    return !!match;
  },

  getFontMarkValue(editor) {
    const [match] = Editor.nodes(editor, {
      match: ({ font }) => typeof font === 'string',
      universal: true,
    });

    return match.font;
  },

  setFontMark(editor, font) {
    Transforms.setNodes(
      editor,
      { font } as CustomText,
      { match: n => Text.isText(n), split: true },
    );
  },

  setFontSizeMark(editor, fontSize) {
    Transforms.setNodes<CustomElement['Element']>(
      editor,
      { fontSize },
      { match: n => Text.isText(n), split: true },
    );
  },
};
