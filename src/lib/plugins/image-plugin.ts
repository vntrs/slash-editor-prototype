import { BaseEditor, Transforms } from 'slate';
import { CustomImageElement } from '../editor-elements';

export function withImages(editor) {
  const { isVoid } = editor;
  editor.isVoid = function (element) {
    if (element.type === 'image') return true;

    return isVoid(element);
  };

  return editor;
}

export const EditorWithImages = {
  insertImage(editor: BaseEditor, url: string) {
    const text = { text: '' };
    const image = {
      image: url,
      type: 'image',
      children: [text],
    } as CustomImageElement;
    Transforms.insertNodes(editor, image);
  },
};
