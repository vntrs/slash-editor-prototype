import { Editor, Transforms } from 'slate';
import { CustomCodeElement } from '../editor-elements';

export const EditorWithCodeBlocks = {
  isCodeBlockActive(editor) {
    const [match] = Editor.nodes(editor, {
      match: ({ type }) => type === 'code',
    });

    return !!match;
  },

  toggleCodeBlock(editor) {
    const isActive = EditorWithCodeBlocks.isCodeBlockActive(editor);
    const type = isActive ? null : 'code';
    const element = { type } as CustomCodeElement;

    Transforms.setNodes(editor, element, {
      match: n => Editor.isBlock(editor, n),
    });
  },
};
