import { Editor, Text, Transforms } from 'slate';
import { CustomText } from '../editor-elements';

export const EditorWithTextStyle = {
  isBoldMarkActive(editor) {
    const [match] = Editor.nodes(editor, {
      match: ({ bold }) => bold === true,
      universal: true,
    });

    return !!match;
  },

  isItalicMarkActive(editor) {
    const [match] = Editor.nodes(editor, {
      match: ({ italic }) => italic === true,
      universal: true,
    });

    return !!match;
  },

  isStrikeMarkActive(editor) {
    const [match] = Editor.nodes(editor, {
      match: ({ strike }) => strike === true,
      universal: true,
    });

    return !!match;
  },

  toggleBoldMark(editor) {
    const isActive = EditorWithTextStyle.isBoldMarkActive(editor);
    Transforms.setNodes(
      editor,
      { bold: isActive ? null : true } as CustomText,
      { match: n => Text.isText(n), split: true },
    );
  },

  toggleItalicMark(editor) {
    const isActive = EditorWithTextStyle.isItalicMarkActive(editor);
    Transforms.setNodes(
      editor,
      { italic: isActive ? null : true } as CustomText,
      { match: n => Text.isText(n), split: true },
    );
  },

  toggleStrikeMark(editor) {
    const isActive = EditorWithTextStyle.isStrikeMarkActive(editor);
    Transforms.setNodes(
      editor,
      { strike: isActive ? null : true } as CustomText,
      { match: n => Text.isText(n), split: true },
    );
  },
};
