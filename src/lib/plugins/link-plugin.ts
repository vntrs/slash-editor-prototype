import { Editor, Text, Transforms } from 'slate';
import { CustomLinkElement } from '../editor-elements';

export const EditorWithLinks = {
  isLinkMarkActive(editor) {
    const [match] = Editor.nodes(editor, {
      match: ({ link }) => typeof link === 'string',
      universal: true,
    });
    return !!match;
  },

  getLinkMarkValue(editor) {
    const [match] = Editor.nodes(editor, {
      match: ({ link }) => typeof link === 'string',
      universal: true,
    });

    return match.link;
  },

  setLinkMark(editor, link: string) {
    Transforms.setNodes(
      editor,
      { link } as CustomLinkElement,
      { match: n => Text.isText(n), split: true },
    );
  },

  removeLinkMark(editor) {
    Transforms.setNodes(
      editor,
      { link: null } as CustomLinkElement,
      { match: n => Text.isText(n), split: true },
    );
  },
};
