import styled from 'styled-components/macro';

const ImageWrapper = styled.div`
  display: flex;
  flex-direction: column;
  max-width: 500px;
  background-color: #c0c0c0;
  padding: 4px 8px;
`;

const Image = styled.img`
  margin-bottom: 4px;
`;

const BottomText = styled.small``;

export const ImageElement = ({ attributes, children, image }) => {
  return (
    <div {...attributes} >
      <ImageWrapper contentEditable={false}>
        <Image src={image} alt={image} />
        <BottomText>{image}</BottomText>
      </ImageWrapper>
      {children}
    </div>
  );
};
