import styled from 'styled-components/macro';

export const Aligned = styled.div`
  text-align: ${({ align }) => align};
`;

export const AlignedElement = ({ attributes, children, align }) => {
  return (
    <Aligned align={align} {...attributes}>
      {children}
    </Aligned>
  );
};
