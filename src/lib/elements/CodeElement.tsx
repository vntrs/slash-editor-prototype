import styled from 'styled-components/macro';

const Code = styled.pre`
  background-color: rgba(0, 0, 0, 0.2);
  margin: 0;
  padding: .5em 1em;
`;

export const CodeElement = ({ attributes, children }) => {
  return (
    <Code {...attributes}>
      <code>{children}</code>
    </Code>
  );
};

