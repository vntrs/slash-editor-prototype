import { useCallback, useMemo } from 'react';
import { DefaultElement } from 'slate-react';

import { CodeElement } from './CodeElement';
import { ImageElement } from './ImageElement';
import { AlignedElement } from './AlignedElement';

export const Leaf = ({ leaf, attributes, children }) => {
  const style = useMemo(() => {
    // Some formatting marks can probably stay as simply css-only changes,
    //  as opposed to changing how the thing is displayed completely (eg. code)
    const fontWeight = leaf.bold ? 'bold' : undefined;
    const fontStyle = leaf.italic ? 'italic' : undefined;
    const textDecoration = leaf.strike ? 'line-through' : undefined;
    const fontFamily = leaf.font ? leaf.font : undefined;
    const fontSize = leaf.fontSize ? Number.parseInt(leaf.fontSize) : undefined;
    return { fontWeight, fontStyle, textDecoration, fontFamily, fontSize };
  }, [leaf]);

  if (leaf.link) {
    return (
      <a {...attributes} target="_blank" href={leaf.link} style={style}>
        {children}
      </a>
    );
  }

  return (
    <span {...attributes} style={style}>
      {children}
    </span>
  );
};

export function useEditorRenderers() {
  const renderElement = useCallback(({ element, children, ...props }) => {
    if (element.align) {
      return (
        <AlignedElement align={element.align}>
          {renderElement({
            element: { ...element, align: null },
            children,
            ...props,
          })}
        </AlignedElement>
      );
    }

    switch (element.type) {
      case 'code':
        return <CodeElement {...props} children={children} />;
      case 'image':
        return <ImageElement {...props} image={element.image} children={children} />;
      default:
        return <DefaultElement {...props} children={children} />;
    }
  }, []);
  const renderLeaf = useCallback((props) => {
    return <Leaf {...props} />;
  }, []);

  return useMemo(
    () => ({ renderElement, renderLeaf }),
    [renderElement, renderLeaf],
  );
}
