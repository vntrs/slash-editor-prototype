export type CustomLeaf = { text: string };
export type LinkLeaf = {
  link: string;
};
export type StyledLeaf = {
  bold?: boolean;
  italic?: boolean;
};
export type FontLeaf = {
  font?: string;
  fontSize?: number;
};

export type CustomParagraphElement = {
  type: 'paragraph';
  children: CustomText[];
}
export type CustomCodeElement = {
  type: 'code';
  children: CustomText[];
};
export type CustomAlignedElement = {
  type: 'aligned';
  align: 'left' | 'center' | 'right' | 'justify';
  children: CustomText[];
};
export type CustomImageElement = {
  type: 'image';
  children: CustomText[];
  image: string;
};
export type CustomLinkElement = {
  text: string;
  link: string;
};

export type CustomText =
  & CustomLeaf
  & LinkLeaf
  & StyledLeaf
  & FontLeaf;
export type CustomElement =
  | CustomParagraphElement
  | CustomAlignedElement
  | CustomCodeElement
  | CustomImageElement
  | CustomLinkElement;
