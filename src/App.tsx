import './App.css';
import styled from 'styled-components/macro';
import { Sidebar } from './components/Sidebar';
import { Editor } from './components/Editor';
import { FileContextProvider } from './context/file-context';
import { EditorContextProvider } from './context/editor-context';

const Container = styled.div`
  width: 100vw;
  height: 100vh;
  background-color: #303030;
  display: flex;
`;

function App() {
  return (
    <FileContextProvider>
      <EditorContextProvider>
        <Container>
          <Sidebar />
          <Editor />
        </Container>
      </EditorContextProvider>
    </FileContextProvider>
  );
}

export default App;
